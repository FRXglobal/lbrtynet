show examples of pwnat in use

Pwnat (Peer-to-World NAT) is a tool designed for establishing a bidirectional connection between two computers that are behind different NATs without the need for any 3rd-party involvement. It uses a sophisticated technique to bypass NAT restrictions.

Example 1: Establishing a Direct Connection Between Two Computers
Assuming you have two computers (Computer A and Computer B) behind different NATs, you can use Pwnat to establish a direct connection between them. Here's an example of how it can be used:

On Computer A:
pwnat -s | ssh user@public_server 'pwnat -c public_server_ip 22'


On Computer B:
pwnat -c public_server_ip 22


In this example, Pwnat is used to establish a direct connection between Computer A and Computer B via a public server using SSH.

Example 2: Establishing a Direct Connection with UDP Hole Punching
Pwnat can also be used for UDP hole punching to establish direct connections between two peers behind NATs. Here's a simplified example:

On Computer A:
pwnat -s | nc -u public_server_ip 4321


On Computer B:
nc -u public_server_ip 4321 | pwnat -c public_server_ip 4321


In this example, UDP hole punching is achieved using Pwnat to establish a direct connection between Computer A and Computer B via a public server.

Please note that the actual usage may vary based on the specifics of the NAT environments and the network configurations. Additionally, always ensure that you have the necessary permissions to perform such operations and comply with all relevant laws and regulations.
