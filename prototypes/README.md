LBRTYnet prototypes
===================

I have mixed feelings about this.  In one sense, it feels like cheating.  In another sense, if I were to hand code all the concepts, it would take forever.  Plus, if I was coding all this over a period of time, I would be re-using code from prior development (which I have, but some of it I would prefer not to have to rework to this project).

So, I am creating prototype code using chatGPT and other AI tools.  It allows me to create quick, proto code, which can then be modified (and fixed), specifically for this project, in order to bootstrap many core functions.

In the event that it is desirable, any code that is used from these prototypes, can be recoded at a future date.  In many cases, that will more than likely be desirable.

Again, the point being MVP in shortest time.

So in the folder are the notes on different concepts and functions to help get this project to where, at least I will be comfortable that it is ready for alpha and beta testing.
