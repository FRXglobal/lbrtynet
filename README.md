LBRTYnet
========

We are continuing to update information and work on getting the core toolset working.  Please be patient if some things aren't working.

The most important part of this LBRTYnet repository (this one) README.md is that the repository also has a wiki set up which will contain the important information on participating specifically in LBRTYnet:

https://gitlab.com/FRXglobal/lbrtynet/-/wikis/home

LBRTYnet is a toolkit and framework for creating the infrastructure and methodology of a private, independent wide area network that runs parallel and is autonomous to what is called "the internet".

More than anything else, the goal with this project is to develop a network that is not managed / controlled by telecom and government.  A network that can function autonomously if the entire "internet" structure was taken down yet can intermingle with, as necessary or desirable, depending on the context.

Further, a goal is to ensure that everyone can host their own content with ease, and contribute to the knowledge and resources available to the general public.

This IS NOT a blockchain project.  This is an infrastructure project.  Blockchain and other distributed communication and function can and will be part of this network, but it is not the foundation of this project.

Those who know their way around git and software development can utilize this main repo in order to check things out.  For others or for more concise information in order to learn how to participate, visit the installation wiki:

https://gitlab.com/FRXglobal/lbrtynet/-/wikis/Installation

NB: There has been many changes over the past couple of years and as such, this repository will be updated to reflect those changes...
