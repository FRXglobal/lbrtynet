bare metal
==========

This (and any sub) folder(s) contain information, source, and scripts (etc) for creating a bare metal install of lbrtynet.  This is independent of the VMs which will be more constrained by automation tools for those who would rather just use the system and are less concerned about what goes on their systems, even considering the sandboxed nature of a VM.

The content in core distribution folders, such as debian, should work in distributions that are based on those cores, such as Ubuntu from debian
