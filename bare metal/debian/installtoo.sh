#!/bin/bash
# installs veilid and softetherVPN
#
# Copyright (C) 2024, FReedom eXchange
#
# Run from userspace, so just reboot system after install.sh

echo " "
echo " "
echo "Be sure you ran install.sh from root first and then rebooted, before running this script"
echo " "

cd ~

# the following installs softetherVPN
mkdir seVPN
cd seVPN
wget https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/holepunch/softether-vpnserver-v4.42-9798-rtm-2023.06.30-linux-x64-64bit.tar.gz
tar -xvf softether-vpnserver-v4.42-9798-rtm-2023.06.30-linux-x64-64bit.tar.gz
rm softether-vpnserver-v4.42-9798-rtm-2023.06.30-linux-x64-64bit.tar.gz
cd vpnserver
/bin/make

# begin full echo command.  cut and paste in full, then press enter
# alternatively, use your preferred editor to put in full and only, the following information within the quotes into
#                 /etc/systemd/system/softether-vpnserver.service

echo "[Unit]

Description=SoftEther VPN server

After=network-online.target

After=dbus.service

[Service]

Type=forking

ExecStart=/opt/softether/vpnserver start

ExecReload=/bin/kill -HUP $MAINPID

[Install]

WantedBy=multi-user.target" >> /etc/systemd/system/softether-vpnserver.service

# end echo

./vpnserver start
cd ..
wget https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/holepunch/softether-vpnclient-v4.42-9798-rtm-2023.06.30-linux-x64-64bit.tar.gz
tar -xvzf softether-vpnclient-v4.42-9798-rtm-2023.06.30-linux-x64-64bit.tar.gz
rm softether-vpnclient-v4.42-9798-rtm-2023.06.30-linux-x64-64bit.tar.gz
cd vpnclient
/bin/make
./vpnclient stop
./vpnclient start

# copy softetherVPN command line tool to seVPN path
cd ..
cp vpnserver/vpncmd vpncmd
cp vpnserver/hamcore.se2 hamcore.se2

echo " "
echo "you can use the following to configure the client and server"
echo "cd seVPN"
echo "sudo ./vpncmd"
echo " "
echo "and you can monitor veilid using the terminal interface"
echo "sudo veilid-cli"

exit
