debian core bare metal
======================

This (and any sub) folder(s) contain information, source, and scripts (etc) for creating a bare metal install of lbrtynet.  This is independent of the VMs which will be more constrained by automation tools for those who would rather just use the system and are less concerned about what goes on their systems, even considering the sandboxed nature of a VM.

The content in core distribution folders, such as this one for debian, should work in distributions that are based on those cores, such as Ubuntu

The install.sh script will need to be run from root.

The installtoo.sh script installs the softetherVPN and the rest from the userspace.

The deb12.5.sources.list is to replace a Debian 12.5 file "sources.list"
Alternatively, just remove or comment out the first line as per the list, if it exists.  View the file for more information
BE SURE to modify your sources.list if running the scripts or you may run into errors

The notes.txt file contains the instructions if you would rather follow a line by line setup
