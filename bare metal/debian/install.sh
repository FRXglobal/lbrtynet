#!/bin/bash
#
# Copyright (C) 2024 FReedom eXchange
#
# This script is the first run from a fresh lbrtynet bare metal device
# It needs to be run from root and should work on debian
#
# The following can be followed to have system autostart certain lbrtynet functions.  ONLY if you wish your bare metal install to follow along in lock step with the VM
#
# part 1 fix sources
#     nano /etc/apt/sources.list
# part 2 install bootup.sh
# wget "https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/installer/pre_first_boot/bootup.sh"
# mv bootup.sh /etc/init.d/bootup.sh
# chmod 755 /etc/init.d/bootup.sh
# /usr/sbin/update-rc.d bootup.sh defaults
#
# Useage:
# There is no useage, this is a standalone script

# necessary update if sources.list changed and to be current
apt update

# installs sudo if desired
apt install sudo -y

# unremark the following if you wish your user to not require passwords for sudo commands
#/usr/sbin/adduser <insert your username without surrounding braces> sudo
#touch /home/<insert your username without surrounding braces>/.first
#echo '<insert your username without surrounding braces> ALL=(ALL) NOPASSWD: ALL' | EDITOR='tee -a' /usr/sbin/visudo

# need to reboot to have sudo functional
# do not need to reboot to complete this script
#/usr/sbin/reboot

# MQTT is necessary for certain message passing for lbrtynet
# apparently no python-mosquitto for debian 12.  not sure if needed
apt-get -y install mosquitto mosquitto-clients dnsutils

# the following installs veilid and dependencies for softetherVPN

# create keyrings path in case it doesn't exist
mkdir /usr/share/keyrings/

# need to install gpg to add veilid keys
apt-get install gpg -y
wget -O- https://packages.veilid.net/gpg/veilid-packages-key.public | gpg --dearmor -o /usr/share/keyrings/veilid-packages-keyring.gpg
dpkg --print-architecture
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/veilid-packages-keyring.gpg] https://packages.veilid.net/apt stable main" | tee /etc/apt/sources.list.d/veilid.list 1>/dev/null
apt-get update

# install veilid
apt install veilid-server veilid-cli -y
systemctl start veilid-server.service
systemctl enable --now veilid-server.service

# need to install packages to make softetherVPN
apt install build-essential gnupg2 gcc make binutils gzip libreadline-dev libssl-dev libncurses5-dev libncursesw5 libpthread-stubs0-dev -y

#
# Run installtoo.sh from userspace, so just reboot system

echo " "
echo " "
echo "Run installtoo.sh from userspace, so just reboot system"
echo " "
echo "system will reboot in five seconds"
echo "if it doesn't manually reboot"

wait 5

/usr/sbin/reboot

exit
