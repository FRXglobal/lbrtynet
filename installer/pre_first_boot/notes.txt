get most used interface name

/usr/sbin/arp -n -H ether | perl -ne 'print $1 if /(\S+\n)$/' | sort | uniq -c | grep -v Iface | sort -n | tail -1 | perl -pe 's/.* //'

show mac address

ip link show enp0s3 | awk '/ether/ {print $2}'

show ip address

ip addr show enp0s3 | awk '/inet / {print $2}' | cut -d/ -f 1



The main NIC will usually have a default route. So:

ip -o -4 route show to default
The NIC:

ip -o -4 route show to default | awk '{print $5}'
The gateway:

ip -o -4 route show to default | awk '{print $3}'
Unlike ifconfig, ip has a consistent & parsable output. It only works on Linux; it won't work on other Unixen.
