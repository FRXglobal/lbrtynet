#!/bin/bash

### BEGIN INIT INFO
# Provides:		pre_init_bootup
# Required-Start:	$remote_fs $syslog
# Required-Stop:	$remote_fs $syslog
# Default-Start:	2 3 4 5
# Default-Stop:		0 1 6
# Short-Description:	lbrtynet VM pre-initialization bootup
# Description: 
#  This bootup script provide a bootstrap to start lbrtynet VM installation
#  runs on boot
#
# example in /etc/rc.local
# /sbin/ip addr | grep inet\ | tail -n1 | awk '{ print $2 }' > /etc/issue
# echo "" >> /etc/issue
#
# make executable
# sudo chmod a+x /etc/rc.local
#
# could use cron
# @reboot /sbin/ip addr | grep inet\ | tail -n1 | awk '{ print $2 }' > /etc/issue && echo "" >> /etc/issue
#
# the problem with cron is removing the line item if it is a "run once" scenario

#
# Copyright (C) 2024 FReedom eXchange
#
# part 1 fix sources
#     nano /etc/apt/sources.list
# part 2 set .noauto with chown frx:frx
# part 3 install bootup.sh
# wget "https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/installer/pre_first_boot/bootup.sh"
# mv bootup.sh /etc/init.d/bootup.sh
# rm /etc/init.d/bootup.sh
# mv ${TEMP_DIR}/afwbootup.sh /etc/init.d/bootup.sh
# chmod 755 /etc/init.d/bootup.sh
# /usr/sbin/update-rc.d bootup.sh defaults
# part 4 unset .noauto
#
### END INIT INFO



# Carry out specific functions when asked to by the system
case "$1" in
  start)
    AUTOOFF_CHECK_PATH="/home/frx/scripts"
    AUTOOFF_CHECK_FILE="/home/frx/.noauto"

#    rm /home/frx/.playlist
#    rm /home/frx/.local
#    rm /home/frx/.network
#    rm /home/frx/.offline
#    rm /home/frx/.newpl
#    rm /home/frx/.nonew
#    sudo -u frx touch /home/frx/.playlist
#    rm /home/frx/syncing
#    rm /home/frx/.omx_playing
#    rm /home/frx/.mkplayrun
#    rm /home/frx/.optimized
#    rm /home/frx/.sysrunning
    
    if [ ! -f "${AUTOOFF_CHECK_PATH}" ]; then
        echo "${AUTOOFF_CHECK_PATH} not found, making available."
        # don't set to blank on install bootup
        # setterm -blank 1
        mkdir ${AUTOOFF_CHECK_PATH}
        chown frx:frx ${AUTOOFF_CHECK_PATH}
        mkdir /home/frx/ctrl
        chown frx:frx /home/frx/ctrl
    fi

# adjusted to ensure network up by waiting 10 seconds
# also starting firstboot.sh which will check via gitlab for current and appropriate versioning
    if [ ! -f "${AUTOOFF_CHECK_FILE}" ]; then
        echo "${AUTOOFF_CHECK_FILE} not found, in auto mode."

        # setting long sleep for testing purposes
        sleep 3
        
#        touch .checknet

        # don't set to blank on install bootup
        # setterm -blank 1
#        /home/frx/scripts/./firstboot.sh &
#
# don't care.  at this point, just set up /usr/sbin/visudo
# user ALL=(ALL) NOPASSWD: ALL
        if [ ! -f "/home/frx/.ready" ]; then
            if [ ! -f "/home/frx/.first" ]; then
                if [ "/home/frx/.deb12vm" ]; then
#                update /etc/apt/sources.list
                    touch .upsrc
                fi
                apt update
                apt install sudo -y
                /usr/sbin/adduser frx sudo
                touch /home/frx/.first
                echo 'frx ALL=(ALL) NOPASSWD: ALL' | EDITOR='tee -a' /usr/sbin/visudo
                /usr/sbin/reboot
            else
                cd /home/frx
                sudo -u frx wget -P /home/frx/scripts/ "https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/src/install%20scripts/bootstrap.sh"
                chmod 755 /home/frx/scripts/bootstrap.sh
                sudo -u frx /home/frx/scripts/./bootstrap.sh &
#                sudo -u frx touch /home/frx/.ready
#                rm first_setup.sh
#                rm /home/frx/.first
            fi
        else
            sudo -u frx /home/frx/scripts/./startup.sh &
        fi
    fi
    echo "Could do more here"
    ;;
  stop)
    echo "Stopping script bootup.sh"
    echo "Could do more here"
    ;;
  *)
    echo "Usage: /etc/init.d/bootup.sh {start|stop}"
    exit 1
    ;;
esac

exit 0
