#!/bin/bash
# installs noo-ebs
#
# Copyright (C) 2021, FReedom eXchange
#
# Obviously, in this first instance of noo-ebs for test and
# production, this script installs MQTT
#
# The goal is to eventually move to, or at least add as a
# supplemental, a peer to peer message queue system
#

# apparently no python-mosquitto for debian 12.  not sure if needed
apt-get -y install mosquitto mosquitto-clients dnsutils

#config public MQTT
echo "allow_anonymous true
listener 1883 0.0.0.0" >> /etc/mosquitto/conf.d/mosquitto.conf
service mosquitto restart

