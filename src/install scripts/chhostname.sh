#!/bin/bash
#
# Script changes hostname
#
# Copyright (C) 2024, FReedom eXchange
#

AEBL_TEST="/home/frx/.aebltest"
AEBL_SYS="/home/frx/.aeblsys"
TEMP_DIR="/home/frx/tmp"
IHDN_TEST="/home/frx/.ihdntest"
IHDN_SYS="/home/frx/.ihdnsys"
IHDN_DET="/home/frx/.ihdndet"

T_STO="/run/shm"
T_SCR="/run/shm/scripts"

LOCAL_SYS="${T_STO}/.local"
NETWORK_SYS="${T_STO}/.network"
OFFLINE_SYS="${T_STO}/.offline"

cd $HOME

#Assign existing hostname to $hostn
hostn=$(cat /etc/hostname)

# read newhost
# Set new hostname $newhost
newhost=$(cat "${HOME}/ctrl/newhost" | head -n1)

#change hostname in /etc/hosts & /etc/hostname
sudo sed -i "s/${hostn}/${newhost}/g" /etc/hosts
sudo sed -i "s/${hostn}/${newhost}/g" /etc/hostname

rm "${HOME}/ctrl/newhost"

exit 0
