#!/bin/bash
# installs veilid and softetherVPN
#
# Copyright (C) 2024, FReedom eXchange
#
# Currently needs to be run from root.
# 
# su into root. Download script.  Make executable.  Run script.
#

# create keyrings path in case it doesn't exist
mkdir /usr/share/keyrings/

mosquitto_pub -d -t lbrtynet/log -m "sources update" -h 192.168.0.107 &

apt-get update

mosquitto_pub -d -t lbrtynet/log -m "installing gpg" -h 192.168.0.107 &

# touch .instgpg
# need to install gpg to add veilid keys
apt-get install gpg -y
wget -O- https://packages.veilid.net/gpg/veilid-packages-key.public | gpg --dearmor -o /usr/share/keyrings/veilid-packages-keyring.gpg
dpkg --print-architecture
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/veilid-packages-keyring.gpg] https://packages.veilid.net/apt stable main" | tee /etc/apt/sources.list.d/veilid.list 1>/dev/null

mosquitto_pub -d -t lbrtynet/log -m "installing veilid" -h 192.168.0.107 &

# touch .instveilid
# install veilid
apt install veilid-server veilid-cli -y
systemctl start veilid-server.service
systemctl enable --now veilid-server.service

mosquitto_pub -d -t lbrtynet/log -m "installing build tools" -h 192.168.0.107 &

# touch .instbuildtools
# need to install packages to make softetherVPN
apt install build-essential gnupg2 gcc make binutils gzip libreadline-dev libssl-dev libncurses5-dev libncursesw5 libpthread-stubs0-dev curl -y

mosquitto_pub -d -t lbrtynet/log -m "installing net tools" -h 192.168.0.107 &

# touch .net-tools
# need to install net-tools for network diagnostics purposes
apt install net-tools -y

mosquitto_pub -d -t lbrtynet/log -m "installing zeroconf" -h 192.168.0.107 &

#install zeroconf like utilities
apt install avahi-daemon avahi-utils libnss-mdns -y

mosquitto_pub -d -t lbrtynet/log -m "installing apache" -h 192.168.0.107 &

# install apache for core interface
apt-get -y install apache2

cd ~
rm first_setup.sh

