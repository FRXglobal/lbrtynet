#!/bin/bash
# installs veilid and softetherVPN
#
# Copyright (C) 2024, FReedom eXchange
#
# NB: change hostname when possible
# /etc/hostname
# /etc/hosts

# example code
# patch system now seeking network info in /run/shm
#LOCAL_SYS=".local"
#NETWORK_SYS=".network"
#OFFLINE_SYS=".offline"
#AEBL_TEST="/home/pi/.aebltest"
#AEBL_SYS="/home/pi/.aeblsys"
#AEBL_VM="/home/pi/.aeblvm"
#IHDN_TEST="/home/pi/.ihdntest"
#IHDN_SYS="/home/pi/.ihdnsys"
#IHDN_DET="/home/pi/.ihdndet"

#TEMP_DIR="/home/pi/tempdir"
#MP3_DIR="/home/pi/mp3"
#MP4_DIR="/home/pi/mp4"
#PL_DIR="/home/pi/pl"
#CTRL_DIR="/home/pi/ctrl"
#BIN_DIR="/home/pi/bin"

FRXmsgnode="192.168.0.107"

USER=`whoami`
CRONLOC=/var/spool/cron/crontabs
CRONCOMMFILE=/tmp/cron_comm_file.sh
#CRONCOMMFILE="${HOME}/testcron.sh"
CRONGREP=$(crontab -l | cat )

#IPe0=$(ip addr show eth0 | awk '/inet / {print $2}' | cut -d/ -f 1)
#MACe0=$(ip link show eth0 | awk '/ether/ {print $2}')

# Discover network availability
#

#ping -c 1 8.8.8.8
#
#if [ $? -eq 0 ]; then
#    touch .network
#    echo "Internet available."
#else
#    rm .network
#fi
#
#ping -c 1 192.168.200.6
#
#if [[ $? -eq 0 ]]; then
#    touch .local
#    echo "Local network available."
#else
#    rm .local
#fi
#
#if [ -f "${LOCAL_SYS}" ] && [ ! -f "${NETWORK_SYS}" ]; then
#    touch .offline
#    echo "No network available."
#else
#    rm .offline
#fi

cd ~

touch /home/frx/.alpha

wget -P /home/frx/ "https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/src/install%20scripts/first_setup.sh"
chmod 755 /home/frx/first_setup.sh

wget -P /home/frx/ "https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/src/noo-ebs/install.sh"
chmod 755 /home/frx/install.sh

mkdir /home/frx/.backup/
wget -P /home/frx/.backup/ "https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/src/install%20scripts/resolv.conf"
chmod 644 /home/frx/.backup/resolv.conf
sudo chown root:root /home/frx/.backup/resolv.conf
sudo rm /etc/resolv.conf            
sudo cp /home/frx/.backup/resolv.conf /etc/resolv.conf

sleep 5

# check DNS update
FRXdns=$(host -v home.frx | awk -F "[ #]" '/Received /{print$5}' | uniq)
     
# create keyrings path in case it doesn't exist
# need to run the following scripts because they need root
cd ~
sudo ./install.sh

# mosquitto example
#   mosquitto_pub -d -t frx/$hostn -m "$(date) : $hostn IPv4 $ext_ip4 IPv6 $ext_ip6" -h "frx.ca"

mosquitto_pub -d -t lbrtynet/log -m "current DNS $FRXdns" -h $FRXmsgnode &

mosquitto_pub -d -t lbrtynet/log -m "installing other" -h $FRXmsgnode &

cd ~
sudo ./first_setup.sh

mosquitto_pub -d -t lbrtynet/log -m "other installed" -h $FRXmsgnode &

#config public MQTT
#sudo echo "allow_anonymous true
#listener 1883 0.0.0.0" >> /etc/mosquitto/conf.d/mosquitto.conf
#sudo service mosquitto restart

cd ~
touch .instvpnserve
# install softetherVPN

mosquitto_pub -d -t lbrtynet/log -m "installing seVPN" -h $FRXmsgnode

mkdir seVPN
cd seVPN
wget https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/holepunch/softether-vpnserver-v4.42-9798-rtm-2023.06.30-linux-x64-64bit.tar.gz
tar -xvf softether-vpnserver-v4.42-9798-rtm-2023.06.30-linux-x64-64bit.tar.gz
rm softether-vpnserver-v4.42-9798-rtm-2023.06.30-linux-x64-64bit.tar.gz
cd vpnserver
/bin/make
echo "[Unit]

Description=SoftEther VPN server

After=network-online.target

After=dbus.service

[Service]

Type=forking

ExecStart=/opt/softether/vpnserver start

ExecReload=/bin/kill -HUP $MAINPID

[Install]

WantedBy=multi-user.target" >> /etc/systemd/system/softether-vpnserver.service
./vpnserver start
cd ..
wget https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/holepunch/softether-vpnclient-v4.42-9798-rtm-2023.06.30-linux-x64-64bit.tar.gz
tar -xvzf softether-vpnclient-v4.42-9798-rtm-2023.06.30-linux-x64-64bit.tar.gz
rm softether-vpnclient-v4.42-9798-rtm-2023.06.30-linux-x64-64bit.tar.gz
cd vpnclient
/bin/make
./vpnclient stop
./vpnclient start
touch .instclient

mosquitto_pub -d -t lbrtynet/log -m "seVPN installed" -h $FRXmsgnode

# copy softetherVPN command line tool to seVPN path
cd ..
cp vpnserver/vpncmd vpncmd
cp vpnserver/hamcore.se2 hamcore.se2

echo " "
echo "you can use the following to configure the client and server"
echo "cd seVPN"
echo "sudo ./vpncmd"
echo " "
echo "and you can monitor veilid using the terminal interface"
echo "sudo veilid-cli"

mosquitto_pub -d -t lbrtynet/log -m "prep for production" -h $FRXmsgnode &

cd ~
rm install.sh
rm scripts/bootstrap.sh
rm first_setup.sh
rm .instvpnserve
cd ~
wget -P /home/frx/scripts/ "https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/src/install%20scripts/startup.sh"
chmod 755 /home/frx/scripts/startup.sh
wget -P /home/frx/scripts/ "https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/src/install%20scripts/run.sh"
chmod 755 /home/frx/scripts/run.sh
wget -P /home/frx/scripts/ "https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/src/install%20scripts/pub.sh"
chmod 755 /home/frx/scripts/pub.sh
wget -P /home/frx/scripts/ "https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/src/install%20scripts/ctrlwtch.sh"
chmod 755 /home/frx/scripts/ctrlwtch.sh
wget -P /home/frx/scripts/ "https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/src/install%20scripts/msgrec.sh"
chmod 755 /home/frx/scripts/msgrec.sh
cd ~
touch .ready
rm .wget-hsts
rm .sudo_as_admin_successful
sudo rm /home/frx/.first

mosquitto_pub -d -t lbrtynet/log -m "apache prep" -h $FRXmsgnode &

# from ap009110
# From note on apache2 firstrun page
#  You should replace this file (located at /var/www/html/index.html) before continuing to operate your HTTP server.
#  also enable cgi-mod
sudo a2enmod cgi
sudo service apache2 restart

# leave temporarily as reference if eventually install php
# sudo apt-get install php5-common libapache2-mod-php5 php5-cli

cd ~
mkdir tmp
wget -N -nd -w 3 -P tmp --limit-rate=50k https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/base_iface/index.html
sudo rm /var/www/html/index.html
sudo mv tmp/index.html /var/www/html/index.html
sudo mkdir /var/www/html/images
wget -N -nd -w 3 -P tmp --limit-rate=50k https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/base_iface/images/AEBL_thumb_00.png
sudo mv tmp/AEBL_thumb_00.png /var/www/html/images/AEBL_thumb_00.png
wget -N -nd -w 3 -P tmp --limit-rate=50k https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/base_iface/images/valid-xhtml10.png
sudo mv tmp/valid-xhtml10.png /var/www/html/images/valid-xhtml10.png

wget -N -nd -w 3 -P tmp --limit-rate=50k https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/base_iface/config.html
sudo rm /var/www/html/config.html
sudo mv tmp/config.html /var/www/html/config.html

wget -N -nd -w 3 -P tmp --limit-rate=50k https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/base_iface/blades.html
sudo rm /var/www/html/blades.html
sudo mv tmp/blades.html /var/www/html/blades.html

wget -N -nd -w 3 -P tmp --limit-rate=50k https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/base_iface/system.html
sudo rm /var/www/html/system.html
sudo mv tmp/system.html /var/www/html/system.html

wget -N -nd -w 3 -P tmp --limit-rate=50k https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/base_iface/about.html
sudo rm /var/www/html/about.html
sudo mv tmp/about.html /var/www/html/about.html
    
# from ap009113
# reference for proper setting of cgi for http interface
# sudo chown root:root /usr/lib/cgi-bin/sysview_cgi.sh

wget -N -nd -w 3 -P tmp --limit-rate=50k https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/base_iface/cgi-bin/patch_cgi.sh
sudo rm /usr/lib/cgi-bin/patch_cgi.sh
sudo mv tmp/patch_cgi.sh /usr/lib/cgi-bin/
sudo chown root:root /usr/lib/cgi-bin/patch_cgi.sh
sudo chmod 0755 /usr/lib/cgi-bin/patch_cgi.sh

wget -N -nd -w 3 -P tmp --limit-rate=50k https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/base_iface/cgi-bin/reboot_cgi.sh
sudo rm /usr/lib/cgi-bin/reboot_cgi.sh
sudo mv tmp/reboot_cgi.sh /usr/lib/cgi-bin/
sudo chown root:root /usr/lib/cgi-bin/reboot_cgi.sh
sudo chmod 0755 /usr/lib/cgi-bin/reboot_cgi.sh

wget -N -nd -w 3 -P tmp --limit-rate=50k https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/base_iface/cgi-bin/showcfg_cgi.sh
sudo rm /usr/lib/cgi-bin/showcfg_cgi.sh
sudo mv tmp/showcfg_cgi.sh /usr/lib/cgi-bin/
sudo chown root:root /usr/lib/cgi-bin/showcfg_cgi.sh
sudo chmod 0755 /usr/lib/cgi-bin/showcfg_cgi.sh

wget -N -nd -w 3 -P tmp --limit-rate=50k https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/base_iface/cgi-bin/shutdown_cgi.sh
sudo rm /usr/lib/cgi-bin/shutdown_cgi.sh
sudo mv tmp/shutdown_cgi.sh /usr/lib/cgi-bin/
sudo chown root:root /usr/lib/cgi-bin/shutdown_cgi.sh
sudo chmod 0755 /usr/lib/cgi-bin/shutdown_cgi.sh

wget -N -nd -w 3 -P tmp --limit-rate=50k https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/base_iface/cgi-bin/system_cgi.sh
sudo rm /usr/lib/cgi-bin/system_cgi.sh
sudo mv tmp/system_cgi.sh /usr/lib/cgi-bin/
sudo chown root:root /usr/lib/cgi-bin/system_cgi.sh
sudo chmod 0755 /usr/lib/cgi-bin/system_cgi.sh

wget -N -nd -w 3 -P tmp --limit-rate=50k https://gitlab.com/FRXglobal/lbrtynet/-/raw/master/base_iface/cgi-bin/sysview_cgi.sh
sudo rm /usr/lib/cgi-bin/sysview_cgi.sh
sudo mv tmp/sysview_cgi.sh /usr/lib/cgi-bin/
sudo chown root:root /usr/lib/cgi-bin/sysview_cgi.sh
sudo chmod 0755 /usr/lib/cgi-bin/sysview_cgi.sh

mosquitto_pub -d -t lbrtynet/log -m "apache ready" -h $FRXmsgnode &

# Add dnsutils to raspbian for future consideration
#if [ ! -f "${AEBL_VM}" ]; then
#    sudo apt-get update && sudo apt-get -y dist-upgrade
#    sudo apt-get -y install dnsutils lsb-release
#fi

# Change default html path from /var/www/html to /var/www for AEBL VM (Ubuntu)
#  includes changing apache default www root
#if [ -f "${AEBL_VM}" ]; then
#
    # move html
#    sudo mv /var/www/html/about.html /var/www/
#    sudo mv /var/www/html/blades.html /var/www/
#    sudo mv /var/www/html/config.html /var/www/
#    sudo mv /var/www/html/index.html /var/www/
#    sudo mv /var/www/html/system.html /var/www/
    
    # move images
#    sudo mkdir /var/www/images
#    sudo mv /var/www/html/images/AEBL_thumb_00.png /var/www/images/
#    sudo mv /var/www/html/images/valid-xhtml10.png /var/www/images/
#    sudo rmdir /var/www/html/images
#    sudo rmdir /var/www/html

    # Copy new apache2 default config file
    # 600 : Only owner can read/write
    # 644 : Only owner can write, others can read
    # 666 : All uses can read/write.
#    sudo mv default /etc/apache2/sites-available/
#    sudo chown root:root /etc/apache2/sites-available/default
#    sudo chmod 0644 /etc/apache2/sites-available/default

    # Enable and disable sites
    # This may not be enough to change default, may need to dig deeper
    # nope, looks like /etc/apache2/apache2.conf has /var/www as granted
#    sudo a2dissite 000-default.conf
#    sudo mv /etc/apache2/sites-available/default /etc/apache2/sites-available/default.conf
#    sudo a2ensite default
    
    # Of course, apache2 needs to restart
#    sudo service apache2 restart
    
    # With sudo apt-get install lsb-release installed on raspbian (ubuntu has)
#
# OS=$(lsb_release -si)        - raspbian shows Debian, ubuntu shows ubuntu
# ARCH=$(uname -m | sed 's/x86_//;s/i[3-6]86/32/')
# VER=$(lsb_release -sr)       - raspbian shows wheezy, ubuntu shows current

mosquitto_pub -d -t lbrtynet/log -m "lbrtynet ready" -h $FRXmsgnode &

# complete and start regular operation
/home/frx/scripts/./startup.sh &
